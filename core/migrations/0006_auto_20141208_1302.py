# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20141208_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rodape',
            name='texto',
            field=models.TextField(help_text=b'Texto que vai aparecer no icone', max_length=20),
        ),
    ]
