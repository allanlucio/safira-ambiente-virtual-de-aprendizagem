# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20141208_1302'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rodape',
            name='texto',
            field=models.CharField(help_text=b'Texto que vai aparecer no icone', max_length=30),
        ),
    ]
