# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20141205_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rodape',
            name='configuracoes_Sistema',
            field=models.ForeignKey(to='core.Configuracoes_Sistema'),
        ),
    ]
