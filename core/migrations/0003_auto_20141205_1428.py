# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

import core.apps.config.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_configuracoes_sistema_bloquear_criacao'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuracoes_sistema',
            name='elementos_rodape',
            field=models.IntegerField(default=10, max_length=2, validators=[core.apps.config.models.validar_quantidadeElementosRodape]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rodape',
            name='configuracoes_Sistema',
            field=models.ForeignKey(to='core.Configuracoes_Sistema', validators=[core.apps.config.models.valida_QuantidadeRodape]),
        ),
    ]
