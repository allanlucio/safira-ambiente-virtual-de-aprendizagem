# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Configuracoes_Sistema',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo_pagInicial', models.CharField(default=b'Texto Padr\xc3\xa3o', max_length=30)),
                ('subTitulo_pagInicial', models.CharField(default=b'Altere-o atrav\xc3\xa9s da interface de ADMIN', max_length=40)),
                ('icone', models.ImageField(upload_to=b'imagens/icone', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rodape',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('icone', models.ImageField(help_text=b'Coloque o icone no tamanho y x y', upload_to=b'imagens/rodape/icone', blank=True)),
                ('texto', models.TextField(help_text=b'Texto que vai aparecer no icone')),
                ('link', models.TextField(help_text=b'Coloque um link para redirecionar para a p\xc3\xa1gina', blank=True)),
                ('configuracoes_Sistema', models.ForeignKey(to='core.Configuracoes_Sistema')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
