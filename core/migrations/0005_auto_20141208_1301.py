# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20141208_1244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rodape',
            name='link',
            field=models.URLField(help_text=b'Coloque um link para redirecionar para a p\xc3\xa1gina', blank=True),
        ),
    ]
