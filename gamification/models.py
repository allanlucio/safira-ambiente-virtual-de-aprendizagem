# -*- coding: utf-8 -*-

from django.core.exceptions import ValidationError
from django.db import models

# Create your models here.
from django.db.models.signals import post_save, pre_save
from django.dispatch.dispatcher import receiver
#Imports do avalia_pares
from avalia_pares.apps.atividades.models import Atividades
from avalia_pares.apps.aluno.models import AlunoPeer
from avalia_pares.apps.disciplina.models import DisciplinaPeer


#Funçao para verificar se o valor passado e negativo
def invalidaNegativo(valor):
    if valor < 0:
        raise ValidationError("Valor negativo não é permitido.")


############INICIO_CLASSES###################

class Badge(models.Model):
    # Atributos da classe
    nome = models.CharField(max_length=80, help_text="Digite um nome para a Badge.")
    descricao = models.CharField(max_length=200, help_text="Digite uma descrição.")

class Avatar(models.Model):
    # Atributos da classe
    aluno = models.ForeignKey(AlunoPeer)
    nickname = models.CharField(max_length=80, help_text="Digite um nome para que possamos te identificar.")
    pontos = models.IntegerField(default=0,validators=[invalidaNegativo])
    imagem = models.ImageField(help_text="Escolha uma imagem para o seu Avatar!")
    listaBadges_FK = models.ManyToManyField('BadgeAvatar_FK')
    quantEstadoFeitos = models.IntegerField(default=0,validators=[invalidaNegativo])
    quantBoss = models.IntegerField(default=0,validators=[invalidaNegativo])

class BadgeAvatar_FK(models.Model):
    # Atributos da classe
    badge_FK = models.ForeignKey(Badge)
    avatar_FK = models.ForeignKey(Avatar)

    class Meta:
        unique_together=('badge_FK', 'avatar_FK',)

class Mundo(models.Model):
    # Atributos da classe
    disciplina = models.ForeignKey(DisciplinaPeer)
    nome = models.CharField(max_length=80, help_text="Digite um nome para o mundo.")
    imagem = models.ImageField(help_text="Escolha uma imagem para o seu Avatar!")
    atividadeMundo_FK = models.ManyToManyField('AtividadeMundo_FK')
    descricao = models.CharField(max_length=200, help_text="Digite uma descrição.")

class AtividadeMundo_FK(models.Model):
    # Atributos da classe
    mundo_FK = models.ForeignKey(Mundo)
    atividade_FK = models.ForeignKey(Atividades)
    ordem = models.IntegerField(default=1)
    tipo = models.BooleanField(default=False, help_text="Escolha um tipo para a atividade.")
    badge = models.ForeignKey(Badge, null=True, blank=True, help_text="Escolha uma badge para a atividade.")
    dificuldade = models.IntegerField(default=1, max_length=5, help_text="Escolha um nível de dificuldade para a atividade.")

    class Meta:
        unique_together=('mundo_FK', 'atividade_FK',)

############FIM_CLASSES###################


@receiver(pre_save,sender=Avatar)
def handler_Avatar(sender,instance,raw,**kwargs):
    if instance.pontos < 0:
        raise ValidationError("Valor negativo não é permitido.")
    if instance.quantEstadoFeitos < 0:
        raise ValidationError("Valor negativo não é permitido.")
    if instance.quantBoss < 0:
        raise ValidationError("Valor negativo não é permitido.")