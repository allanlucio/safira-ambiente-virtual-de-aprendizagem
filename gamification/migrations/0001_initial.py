# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gamification.models


class Migration(migrations.Migration):

    dependencies = [
        ('disciplina', '__first__'),
        ('ava', '0006_auto_20150622_1044'),
        ('aluno', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='AtividadeMundo_FK',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ordem', models.IntegerField(default=1)),
                ('tipo', models.BooleanField(default=False, help_text=b'Escolha um tipo para a atividade.')),
                ('dificuldade', models.IntegerField(default=1, help_text=b'Escolha um n\xc3\xadvel de dificuldade para a atividade.', max_length=5)),
                ('atividade_FK', models.ForeignKey(to='ava.Atividades')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Avatar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nickname', models.CharField(help_text=b'Digite um nome para que possamos te identificar.', max_length=80)),
                ('pontos', models.IntegerField(default=0, validators=[gamification.models.invalidaNegativo])),
                ('imagem', models.ImageField(help_text=b'Escolha uma imagem para o seu Avatar!', upload_to=b'')),
                ('quantEstadoFeitos', models.IntegerField(default=0, validators=[gamification.models.invalidaNegativo])),
                ('quantBoss', models.IntegerField(default=0, validators=[gamification.models.invalidaNegativo])),
                ('aluno', models.ForeignKey(to='aluno.AlunoPeer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Badge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(help_text=b'Digite um nome para a Badge.', max_length=80)),
                ('descricao', models.CharField(help_text=b'Digite uma descri\xc3\xa7\xc3\xa3o.', max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BadgeAvatar_FK',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('avatar_FK', models.ForeignKey(to='gamification.Avatar')),
                ('badge_FK', models.ForeignKey(to='gamification.Badge')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Mundo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(help_text=b'Digite um nome para o mundo.', max_length=80)),
                ('imagem', models.ImageField(help_text=b'Escolha uma imagem para o seu Avatar!', upload_to=b'')),
                ('descricao', models.CharField(help_text=b'Digite uma descri\xc3\xa7\xc3\xa3o.', max_length=200)),
                ('atividadeMundo_FK', models.ManyToManyField(to='gamification.AtividadeMundo_FK')),
                ('disciplina', models.ForeignKey(to='disciplina.DisciplinaPeer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='badgeavatar_fk',
            unique_together=set([('badge_FK', 'avatar_FK')]),
        ),
        migrations.AddField(
            model_name='avatar',
            name='listaBadges_FK',
            field=models.ManyToManyField(to='gamification.BadgeAvatar_FK'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='atividademundo_fk',
            name='badge',
            field=models.ForeignKey(blank=True, to='gamification.Badge', help_text=b'Escolha uma badge para a atividade.', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='atividademundo_fk',
            name='mundo_FK',
            field=models.ForeignKey(to='gamification.Mundo'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='atividademundo_fk',
            unique_together=set([('mundo_FK', 'atividade_FK')]),
        ),
    ]
