from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'avaliacao_pares.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('core.apps.config.urls')),
    url(r'^av_pares/',include('avalia_pares.urls')),
    url(r'^usuarios/', include('core.apps.autenticar.urls')),
)
