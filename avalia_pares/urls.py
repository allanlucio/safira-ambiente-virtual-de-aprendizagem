__author__ = 'allan'

from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'avaliacao_pares.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^aluno/', include('avalia_pares.apps.aluno.urls')),
    url(r'^professor/', include('avalia_pares.apps.professor.urls')),
    url(r'^disciplina/', include('avalia_pares.apps.disciplina.urls')),
    url(r'^atividade/', include('avalia_pares.apps.atividades.urls')),

)


