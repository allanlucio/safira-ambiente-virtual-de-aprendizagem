# -*- coding: utf-8 -*-
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import PasswordChangeForm

from django.http.response import HttpResponse

from django.shortcuts import render

from avalia_pares.apps.atividades.models import AtividadesPeer, AtividadeSubmetidas, Criterio, Resposta
from avalia_pares.apps.disciplina.models import DisciplinaPeer

from core.apps.ava.decorators import matriculado_disc_atividade, is_aluno
from core.apps.ava.forms import AtualizarAluno
from core.apps.autenticar.decorators import matriculado_disc
from avalia_pares.apps.atividades.forms import subAtvdForm

from core.apps.ava.models import AlunoDisciplina


@login_required
@is_aluno()

@login_required
@is_aluno()
def home_aluno(request):
    return render(request,'home_aluno.html')

def aluno_disciplinas(request):
    aluno=request.user.aluno
    disciplinas=AlunoDisciplina.objects.filter(aluno=aluno)


    return render(request,'aluno_disciplinas.html',{'disciplinas':disciplinas})





@login_required
@is_aluno()
@matriculado_disc
def aluno_disciplina_vis(request,slug):

    disciplina=DisciplinaPeer.objects.get(slug=slug)
    return render(request, 'detalhe_disc.html',{'detail':disciplina})

@login_required
@is_aluno()
def aluno_atividades(request):

    aluno=request.user.aluno
    aluno_disc=AlunoDisciplina.objects.filter(aluno=aluno,pendencia=False)



    return render(request,'aluno_atividades.html',{'disciplinas':aluno_disc})


@login_required
@is_aluno()
@matriculado_disc
def aluno_disciplina_atividades(request,slug_disc):
    #disciplina=Disciplina.objects.get(slug=slug_disc)
    atividades=AtividadesPeer.objects.filter(disciplina__slug=slug_disc)


    return render(request,'aluno_disciplina_atvd.html',{'atividades':atividades})

@login_required
@is_aluno()
@matriculado_disc_atividade
def aluno_vis_atividade(request,slug,slug_disc):

    atividade=AtividadesPeer.objects.get(slug=slug)


    return render(request, 'detalhe_atividade.html',{'atividade':atividade})


# ============  VIEWs PROVISORIAs ================= #

def detalhe_atividade(request):
    return render(request, 'detalhe_atividade.html')

def detalhe_disc(request):
    return render(request, 'detalhe_disc.html')




# ================================================== #


@login_required
@is_aluno()
@matriculado_disc_atividade
def aluno_submete_atvd(request,slug,slug_disc):


    if request.method=='POST':
        form=subAtvdForm(request.POST,request.FILES)
        if form.is_valid():
            aluno=request.user.aluno
            ativid=AtividadesPeer.objects.get(atividade__slug=slug)
            atvsub=form.save(commit=False)

            try:
                atividadesub=AtividadeSubmetidas.objects.get(aluno=aluno,atividade=ativid)
                if atividadesub:
                    atividadesub.arquivo.delete()
                    atividadesub.arquivo=atvsub.arquivo
                    msg=atividadesub.submeter()
            except:
                atvsub.atividade=ativid
                atvsub.aluno=aluno
                msg=atvsub.submeter()

            return HttpResponse(msg)


    criterios=Criterio.objects.filter(atividade__slug=slug)
    return render(request,'submeter_atividade.html',{'form':subAtvdForm(),'criterios':criterios})

#chamada para atualizar os dados do aluno que esta atualmente logado
@login_required
@is_aluno()
def update_perfil(request):

    aluno=request.user.aluno
    form_user=AtualizarAluno(request.POST or None,instance=aluno)
    form=PasswordChangeForm(request.POST or None)



    if request.method=='POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form_user.is_valid():
            user=form_user.save()

            return HttpResponse("Perfil atualizado com sucesso %s"%user)
        elif form.is_valid():

            form.save()
            # Updating the password logs out all other sessions for the user
            # except the current one if
            # django.contrib.autenticar.middleware.SessionAuthenticationMiddleware
            # is enabled.
            update_session_auth_hash(request, form.user)
            return HttpResponse("Senha atualizada com sucesso")

    return render(request,'atualiza_perfil.html',{'form':form,'user_form':form_user})


@login_required
@is_aluno()
#View para listar todas as atividades que o aluno precisa avaliar.
def lista_atividades_corrigir(request,slug,slug_disc):

    aluno=request.user.aluno
    #respostar que o aluno ira avaliar
    respostas_avaliador=Resposta.objects.filter(avaliador=aluno,atividade__atividade__slug=slug)
    #Respostas em que o aluno está sendo avaliado
    respostas_aluno=Resposta.objects.filter(atividade__aluno=aluno,atividade__atividade__slug=slug)

    return render(request,'lista_correcao_atividade.html',{'respostas_avaliador':respostas_avaliador,'respostas_aluno':respostas_aluno})

@login_required
@is_aluno()
def aluno_comentarioEnotas(request,slug):
    resposta=Resposta.objects.get(slug=slug)
    return render(request, 'aluno_comentarioEnotas.html',{'resposta':resposta})