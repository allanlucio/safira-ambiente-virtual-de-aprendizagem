from django.conf.urls import patterns, url

from avalia_pares.apps.aluno.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'avaliacao_pares.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^home$', home_aluno,name='home_aluno'),
    url(r'^disciplinas$', aluno_disciplinas,name='aluno_disciplinas'),
    url(r'^atividades$', aluno_atividades,name='aluno_atividades'),
    url(r'^update$', update_perfil,name='atualiza_perfil'),
    url(r'^(?P<slug>[a-zA-Z0-9-]+)$', aluno_disciplina_vis,name='aluno_disciplina'),

    url(r'^detalhe_atividade$', detalhe_atividade,name='detalhe_atividade'),

    url(r'^(?P<slug_disc>[a-zA-Z0-9-]+)/atividades$',aluno_disciplina_atividades,name='aluno_disc_atividades'),
    url(r'^(?P<slug_disc>[a-zA-Z0-9-]+)/atividade/(?P<slug>[a-zA-Z0-9-]+)$',aluno_vis_atividade,name='aluno_vis_atividade'),
    url(r'^(?P<slug_disc>[a-zA-Z0-9-]+)/submeter/(?P<slug>[a-zA-Z0-9-]+)$',aluno_submete_atvd,name='aluno_sub_atividade'),
    url(r'^(?P<slug_disc>[a-zA-Z0-9-]+)/lista/(?P<slug>[a-zA-Z0-9-]+)$',lista_atividades_corrigir,name='lista_atividades'),
    url(r'^correcao/(?P<slug>[a-zA-Z0-9-]+)$', aluno_comentarioEnotas,name='aluno_comentarioEnotas'),




)

