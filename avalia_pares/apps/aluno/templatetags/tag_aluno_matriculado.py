# # -*- coding: utf-8 -*-
from core.apps.ava.models import AlunoDisciplina

__author__ = 'allan'

from django import template

register = template.Library()

#Retorna um botão de acordo com a relação do aluno com a disciplina
@register.filter
def aluno_matriculado(user,disciplina):
    #Se o try funcionar é porque o aluno tem alguma relação com disciplina
    try:
        disciplinas= AlunoDisciplina.objects.get(aluno=user.aluno,disciplina=disciplina,pendencia=False)
        return True
    except:
        return False






