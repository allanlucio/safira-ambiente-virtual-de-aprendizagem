from avalia_pares.apps.atividades.models import Resposta

__author__ = 'allan'

from django import template

register = template.Library()

@register.assignment_tag
def avaliado_or_avaliador(user,resp_id):
    aluno=user.aluno
    resposta=Resposta.objects.get(pk=resp_id)
    atividade=resposta.atividade
    if aluno == resposta.avaliador and aluno == atividade.aluno:
        return "avaliador_e_avaliado"
    elif aluno == resposta.avaliador:
        return "avaliador"

    return "avaliado"

