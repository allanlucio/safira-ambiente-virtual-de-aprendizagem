from avalia_pares.apps.atividades.models import Notas

__author__ = 'allan'

from django import template

register = template.Library()

@register.filter()
def get_nota(criterio,resp_slug):
    nota=Notas.objects.get(criterio=criterio,resposta__slug=resp_slug)
    return nota.nota

