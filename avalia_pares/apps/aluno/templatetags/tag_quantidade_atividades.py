#encoding: utf-8
from avalia_pares.apps.atividades.models import AtividadeSubmetidas
from core.apps.ava.models import AlunoDisciplina
from django import template

register = template.Library()

@register.filter
def qtd_atividades(user):
    aluno=user.aluno
    disc_aluno=AlunoDisciplina.objects.filter(aluno=aluno)
    cont=0
    for disc in disc_aluno:
        for atvd in disc.disciplina.atividades_set.all():
            try:
                AtividadeSubmetidas.objects.get(aluno=aluno,atividade__atividade=atvd)
            except:
                cont +=1

    return cont
