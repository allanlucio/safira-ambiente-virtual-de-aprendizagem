# # -*- coding: utf-8 -*-
from django.utils.safestring import mark_safe
from avalia_pares.apps.disciplina.models import DisciplinaPeer
from core.apps.ava.models import AlunoDisciplina

__author__ = 'allan'

from django import template
from django.core.urlresolvers import reverse

register = template.Library()

#Retorna um botão de acordo com a relação do aluno com a disciplina
@register.filter
def rel_aluno_disc(user,disciplina):
    #Se o try funcionar é porque o aluno tem alguma relação com disciplina
    try:
        disciplinas= AlunoDisciplina.objects.get(aluno=user.aluno,disciplina=disciplina)

    except:
        #Se alguma exceção for lançada significa que o aluno não possui relação com disciplina, ou seja está desmatriculado
        disciplina=DisciplinaPeer.objects.get(disciplina__pk=disciplina)
        string=reverse(viewname='solicita_disciplina',kwargs={'slug':disciplina.disciplina.slug})

        return mark_safe("<a href=\"%s\" class=\"btn btn-info\">Solicitar Matrícula</a>"%string)

    #Se a pendencia for True o Aluno já solicitou matricula mas o professor ainda não viu
    if disciplinas.pendencia==True:
        return mark_safe('<a href=\"#\" class=\"btn btn-warning\" disabled>Aguardando aprovação</a>')
    else:
        #se Pendência for False o aluno já está devidamente matriculado na disciplina
        string = reverse(viewname='detalhe_disciplinas', kwargs={'slug':disciplinas.disciplina.slug})
        return mark_safe('<a href=\"%s\" class=\"btn btn-success\" disabled>Matriculado</a>'%string)
