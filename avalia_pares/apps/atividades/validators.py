# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError

__author__ = 'allan'

#Validar a nota se está dentro dos valores permitidos
def valida_nota(value):

    value=float(value)
    if value> 10.0 or value <0:
        raise ValidationError("Notas devem ter valores entre 0 e 10!")

def valida_peso(value):
    if value<0.0 or value >100.0:
        raise ValidationError("Pesos devem ter valor entre 0 e 100")