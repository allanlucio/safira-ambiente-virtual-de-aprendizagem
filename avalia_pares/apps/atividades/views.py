from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.core import serializers
from django.db import transaction
from avalia_pares.apps.atividades.models import AtividadesPeer, Resposta, Notas
from core.apps.ava.decorators import is_professor, is_aluno


@login_required
@is_professor()
def detalhe_atividade(request,slug):
    atividade=AtividadesPeer.objects.get(atividade__slug=slug)
    return render(request, 'detalhe_atividade.html',{'atividade':atividade})


@login_required
@is_aluno()
def posta_comentario(request,slug):
    if request.method == 'POST':
        aluno=request.user.aluno
        resposta=Resposta.objects.get(slug=slug)
        comentario=request.POST['comentario']
        msg=resposta.posta_comentario(comentario=comentario,aluno=aluno)

        return redirect(reverse(viewname='aluno_comentarioEnotas',kwargs={'slug':slug}))
    return HttpResponseRedirect("/home")


@login_required
@is_aluno()
def posta_replica(request,slug):

    if request.method == 'POST':
        aluno=request.user.aluno
        resposta=Resposta.objects.get(slug=slug)
        replica=request.POST['replica']
        msg=resposta.posta_replica(replica=replica,aluno=aluno)

        return redirect(reverse(viewname='aluno_comentarioEnotas',kwargs={'slug':slug}))
    return HttpResponseRedirect("/home")

@login_required
@is_aluno()
def posta_treplica(request,slug):

    if request.method == 'POST':
        aluno=request.user.aluno
        resposta=Resposta.objects.get(slug=slug)

        treplica=request.POST['treplica']

        msg = resposta.posta_treplica(treplica=treplica,aluno=aluno)

        return redirect(reverse(viewname='aluno_comentarioEnotas',kwargs={'slug':slug}))

    return HttpResponseRedirect("/home")

@login_required
@is_aluno()
@transaction.atomic
def posta_notas(request,slug):
    if request.method=='POST':
        #notas=request.POST.get('nota')
        #criterios=request.POST.get('criterio')

        notas=request.POST.getlist('nota')
        criterios=request.POST.getlist('criterio')
        for cont in range(0,len(notas)):

            nota=Notas.objects.get(criterio=criterios[cont],resposta__slug=slug)

            nota.nota=notas[cont].replace(",",".")
            nota.inserir_nota(request.user.aluno)

        return redirect(reverse(viewname='aluno_comentarioEnotas',kwargs={'slug':slug}))
    return HttpResponseRedirect("/home")

@login_required
@is_aluno()
def posta_nota_avaliador(request,slug):
    if request.method=='POST':
        #notas=request.POST.get('nota')
        #criterios=request.POST.get('criterio')

        nota=request.POST['avaliar_revisor']
        aluno=request.user.aluno
        resposta=Resposta.objects.get(slug=slug)

        msg = resposta.posta_nota_avaliador(nota=nota,aluno=aluno)

        return redirect(reverse(viewname='aluno_comentarioEnotas',kwargs={'slug':slug}))




    return HttpResponseRedirect("/home")


#Notas idividuais de cada alunos
@login_required
@is_professor()
def getNotasAluno(request,slug_atividade):


    notas = Notas.objects.filter(resposta__atividade__slug=slug_atividade)
    data = serializers.serialize("json", notas)
    return HttpResponse(data, content_type='application/json')
