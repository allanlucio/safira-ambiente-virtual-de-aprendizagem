from django import forms
from django.forms.widgets import DateTimeInput, HiddenInput
from avalia_pares.apps.atividades.models import AtividadesPeer, Resposta, AtividadeSubmetidas


class DateInputWidget(DateTimeInput):
    def __init__(self, attrs=None, date_format=None, time_format=None):
        attrs=({
            'class':'form-control',
            'type':'datetime'
        })


        super(DateTimeInput, self).__init__(attrs=attrs)





class subAtvdForm(forms.ModelForm):
    class Meta:
        model=AtividadeSubmetidas
        fields=['arquivo']

class Comentar(forms.ModelForm):
    class Meta:
        model=Resposta
        fields=['comentario']



class CreateAtividade(forms.ModelForm):
    class Meta:
        model=AtividadesPeer
        #fields=['titulo','arquivo','fase_Inicio','fase_submeter','fase_comentar','fase_replica','fase_treplica','fase_nota','fase_finalizada','slug']

        widgets={
            'slug':HiddenInput(),'fase_Inicio':DateInputWidget(),'fase_submeter':DateInputWidget(),'fase_comentar':DateInputWidget(),'fase_replica':DateInputWidget(),'fase_treplica':DateInputWidget(),'fase_nota':DateInputWidget(),'fase_finalizada':DateInputWidget()
         }

    def clean_arquivo(self):
        cleaned_data = super(CreateAtividade, self).clean()
        file = cleaned_data.get('arquivo')

        if file:
            filename = file.name
            print filename
            if filename.endswith('.pdf'):
                pass
            else:

                raise forms.ValidationError("Aceitamos Arquivos apenas no formato PDF")

        return file