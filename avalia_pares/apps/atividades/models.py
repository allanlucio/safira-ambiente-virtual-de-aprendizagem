# -*- coding: utf-8 -*-

import hashlib
import os
import random

from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models.signals import pre_save, post_save, pre_delete
from django.dispatch.dispatcher import receiver
from django.utils import timezone
from django.db.models import Sum
from avalia_pares.apps.atividades.utils import media, mediana, conv_date_data, conv_date_horas
from avalia_pares.apps.atividades.validators import valida_nota, valida_peso
from core.apps.ava.models import Aluno, Atividades
from django.db import models








#Modelo de atividades
class AtividadesPeer(models.Model):
    atividade=models.ForeignKey(Atividades)
    fase_Inicio=models.DateTimeField(verbose_name="Data De Inicio da atividade")
    fase_submeter=models.DateTimeField(verbose_name="Data limite para submissão")
    fase_comentar=models.DateTimeField(verbose_name="Inicio do comentário")
    fase_replica=models.DateTimeField(verbose_name="Inicio da Réplica")
    fase_treplica=models.DateTimeField(verbose_name="Inicio da tréplica")
    fase_nota=models.DateTimeField(verbose_name="Inicio da postagem de notas")
    fase_finalizada=models.DateTimeField(verbose_name="Data Final da atividade")
    def __unicode__(self):

        return self.titulo
    def get_absolute_url(self):

        return reverse(viewname='aluno_vis_atividade',kwargs={'slug':self.slug,'slug_disc':self.disciplina.slug})

    def get_submeter_url(self):

        return reverse(viewname='aluno_sub_atividade',kwargs={'slug':self.slug,'slug_disc':self.disciplina.slug})


    #Aloca as respostas, ou seja, cria um espaço para que possa haver comentario, replica e treplica de uma atividade
    @transaction.atomic
    def aloca_atividades(self,atividadesSubmetidas=False):

        #Determina o intervalo de indices para quantidade de avaliadores par
        if self.disciplina.quantidade_de_avaliadores%2==0:
            nr_avaliadores=self.disciplina.quantidade_de_avaliadores/2-1
            min=-1*(nr_avaliadores)-1
            print "par",nr_avaliadores,min

        #Determina o intervalo de indice para quantidade de avalaliadores impares
        else:
            nr_avaliadores=self.disciplina.quantidade_de_avaliadores/2
            min=-1*(nr_avaliadores)

        index=0

        if not atividadesSubmetidas:
            atividadesSubmetidas=AtividadeSubmetidas.objects.filter(atividade=self).order_by('?')
        #Valor negativo, a esquerda do base para pegar os avaliadores

        for atvd in atividadesSubmetidas:


            for pos in range(min,nr_avaliadores+1):
                print pos
                avaliador=atividadesSubmetidas[(index-pos)%len(atividadesSubmetidas)].aluno
                resposta=Resposta()
                resposta.atividade=atvd
                resposta.avaliador=avaliador

                resposta.save()


            index+=1
        self.aloca_nota_atividades()

    #Relaciona notas as respostas de acordo com os criterios da atividade
    @transaction.atomic
    def aloca_nota_atividades(self):
        resposta=Resposta.objects.filter(atividade__atividade=self)
        criterio=Criterio.objects.filter(atividade=self)
        for resp in resposta:

            for crit in criterio:
                nota=Notas(resposta=resp,criterio=crit)
                nota.save()





#Modelo para comportar as atividades submetidas
class AtividadeSubmetidas(models.Model):
    nota_final_media=models.FloatField(blank=True,null=True,validators=[valida_nota],max_length=5,default=0)
    nota_final_mediana=models.FloatField(blank=True,null=True,validators=[valida_nota],max_length=5,default=0)
    arquivo=models.FileField(upload_to='atividades/arquivos/atividades/submetidas')
    aluno=models.ForeignKey(Aluno)
    atividade=models.ForeignKey(Atividades)
    data_submissao=models.DateTimeField(auto_now=True,editable=False)
    slug=models.SlugField(max_length=40,db_index=True,blank=True,unique=True)
    class Meta:
        unique_together=('aluno','atividade')
    def __unicode__(self):
        return self.aluno.nome

    #Calcula a nota total do estudante
    @transaction.atomic
    def nota_total(self,forcar=False):
        total=[]
        respostas=Resposta.objects.filter(atividade=self)
        for resp in respostas:

            total.append(resp.nota_total)

        penitencia=self.calc_penalidade()
        #Aplicando a penitencia na media
        med=media(total)
        med=med-(med*penitencia)/100.0

        #Aplicando a penitencia na mediana
        median=mediana(total)
        median=median-(median*penitencia)/100.0
        self.nota_final_media=med

        self.nota_final_mediana=median

        self.save()
        return self.nota_final_mediana

    #calcula a penalidade com base na penalidade definida na disciplina da atividade
    def calc_penalidade(self):
        penalidade=self.atividade.disciplina.penalidade
        resp=Resposta.objects.filter(avaliador=self.aluno,atividade__atividade=self.atividade)
        resp2=Resposta.objects.filter(atividade__atividade=self.atividade,atividade__aluno=self.aluno)
        tot=0
        for r in resp:
            if r.comentario == None:
                tot+=1
            if r.treplica == None:
                tot+=1

        for r in resp2:
            if r.replica==None:
                tot+=1
        penalidade*=tot

        if penalidade>100:
            penalidade=100

        return penalidade
    #Submissão entre as datas corretas
    def submeter(self):

        if self.atividade.fase_Inicio >= timezone.now():
            return "Só será permito submissão a partir de: %s de %s"%(conv_date_horas(self.atividade.fase_Inicio),conv_date_data(self.atividade.fase_Inicio))
        elif timezone.now() <=self.atividade.fase_submeter:
            self.save()
            return "Enviado com sucesso"

        else:
            return "O tempo para submeter expirou na hora: %s de %s"%(conv_date_horas(self.atividade.fase_submeter),conv_date_data(self.atividade.fase_submeter))

#modelo de notas, onde é referenciado uma resposta, pelo motivo de ter uma nota pra cada criterio relacionado com uma correção
class Notas(models.Model):
    nota=models.FloatField(blank=True,validators=[valida_nota],null=True,default=0)
    resposta=models.ForeignKey('Resposta')
    criterio=models.ForeignKey('Criterio')
    data_post=models.DateTimeField(auto_now_add=True,editable=False)
    def __unicode__(self):
        return "Atividade: %s avaliador: %s avaliado: %s"%(self.criterio.atividade,self.resposta.avaliador,self.resposta.atividade.aluno)

    def inserir_nota(self,aluno):
        if self.resposta.avaliador == aluno:
            atividade=self.resposta.atividade.atividade
            data_hoje=timezone.now()
            if atividade.fase_treplica <= data_hoje <= atividade.fase_nota:
                self.save()
                return "nota inserida com sucesso"

        raise Exception("ainda não é dia para postagem de notas!")




#Criterios estão relacionados com atividades, o administrador pode cadastrar n criterios.
class Criterio(models.Model):
    peso=models.PositiveSmallIntegerField(validators=[valida_peso])
    descricao=models.TextField(max_length=250)
    atividade=models.ForeignKey(Atividades)

    def __unicode__(self):
        return "criterio: %s  | peso =%s   | atividade =%s"%(self.descricao,self.peso,self.atividade)
    #Função para insersão de critério com cuidado para não inserir onde não é possível
    def inserir_criterio(self):
        max=100 #somatorio do peso maximo
        atual=0
        self.atividade.criterio_set.aggregate(Sum('peso'))
        criterios=Criterio.objects.filter(atividade=self.atividade)
        for crit in criterios:
            atual+=crit.peso
        #verifica se a soma dos pesos de todos os criterios é menor ou igual a 100
        if self.peso <= max-atual and atual <=max:
            self.save()
            return "Criterio inserido com sucesso"
        else:
            return "Criterio está com peso maior do que o possivel - a soma de todos os critérios deve ser igual a 100"


# class Fase(models.Model):
#     SUBMISSAO='sub'
#     POST_COMENT='p_coment'
#     POST_NOTA='p_nota'
#     CHOICES=((SUBMISSAO,"Submissão"),(POST_COMENT,'Comentários'),(POST_NOTA,"Postar Notas"))
#
#     tipo=models.CharField(choices=CHOICES,max_length=15)
#     data_inicio=models.DateTimeField()
#     data_fim=models.DateTimeField()
#     descricao=models.TextField(max_length=150)
#     atividade=models.ForeignKey(Atividades,related_name="Atividade_Fase")
#     def __unicode__(self):
#         return self.descricao

#Toda atividade submetida terá o numero de respostas igual ao de avaliadores.
class Resposta(models.Model):
    nota_total=models.FloatField(default=0,verbose_name="Nota total do aluno avaliado",blank=True,max_length=2,null=True,validators=[valida_nota])
    nota_para_avaliador=models.FloatField(default=0,blank=True,null=True,max_length=2,validators=[valida_nota])
    avaliador=models.ForeignKey(Aluno,related_name='Avaliador')
    atividade=models.ForeignKey(AtividadeSubmetidas,related_name="Atividade Submetida")
    comentario=models.CharField(max_length=200,blank=True,null=True)
    replica=models.CharField(max_length=200,blank=True,null=True)
    treplica=models.CharField(max_length=200,blank=True,null=True)
    slug=models.SlugField(max_length=40,db_index=True,blank=True,null=True,unique=True)

    class Meta:
        unique_together=('avaliador','atividade',)

    def __unicode__(self):
        return "Avaliador=%s [Avaliado: %s]"%(self.avaliador,self.atividade.aluno)
    #Calcula a nota da resposta, baseado em cada nota/critério
    @transaction.atomic
    def calcula_nota(self,forcar=False):

        self.nota_total=0
        notas = self.notas_set.all()

        for nota in notas:

            self.nota_total+=(nota.nota * nota.criterio.peso)/100.0

        self.save()
        return self.nota_total

    #postar comentarios
    def posta_comentario(self,comentario,aluno):
        data_atual=timezone.now()
        if self.atividade.atividade.fase_submeter<=data_atual<=self.atividade.atividade.fase_comentar and aluno== self.avaliador:
            self.comentario=comentario
            self.save()
            return "comentário postado com sucesso"
        else:
            return "Postagem não permitida, verifique os horarios"

    #função para postar comentario, com tratamento de erros.
    def posta_replica(self,replica,aluno):
        data_atual=timezone.now()
        avaliado=self.atividade.aluno
        if self.atividade.atividade.fase_comentar<=data_atual<=self.atividade.atividade.fase_replica and aluno== avaliado:
            self.replica=replica
            self.save()
            return "replica postada com sucesso"
        else:
            return "Postagem não permitida, verifique os horarios"
    #Postagem segura de tréplica
    def posta_treplica(self,treplica,aluno):
        data_atual=timezone.now()

        if self.atividade.atividade.fase_replica<=data_atual<=self.atividade.atividade.fase_treplica and aluno== self.avaliador:
            self.treplica=treplica
            self.save()
            return "Tréplica postada com sucesso"
        else:
            return "Postagem não permitida, verifique os horarios"

    #Esta nota é a do aluno avaliado em cima da resposta do avaliador.
    def posta_nota_avaliador(self,nota,aluno):
        data_atual=timezone.now()
        if self.atividade.atividade.fase_treplica<=data_atual<=self.atividade.atividade.fase_nota and aluno== self.atividade.aluno:
            self.nota_para_avaliador=nota
            self.save()
            return "Nota postada com sucesso"
        else:
            return "Postagem não permitida, verifique os horarios"





#handlers de pre_save, atualizam o campo slug a cada vez que uma model é salva











#Deleta o arquivo relacionado coma submissao do aluno
@receiver(pre_delete,sender=AtividadeSubmetidas)
def handler_delete_file_atividade_submetida(sender,instance,**kwargs):
    instance.arquivo.delete()

#preenche o slug em formato de token para as atividades submetidas
@receiver(post_save,sender=AtividadeSubmetidas)
def handler_slug_atividade_submetida(sender,instance,**kwargs):

    if not instance.slug:
        salt=hashlib.sha1(str(random.random())).hexdigest()[:10]
        salt2=hashlib.sha1(str(random.random())).hexdigest()[:10]

        url=hashlib.sha1(salt+conv_date_horas(instance.data_submissao)+salt2+conv_date_data(instance.data_submissao)).hexdigest()



        instance.slug=url
        instance.save()


#preenche o slug em formato de token para as atividades submetidas
@receiver(post_save,sender=Resposta)
def handler_slug_resposta(sender,instance,**kwargs):
    if not instance.slug:
        salt=hashlib.sha1(str(random.random())).hexdigest()[:20]
        salt2=hashlib.sha1(str(random.random())).hexdigest()[:20]

        url=hashlib.sha1(salt+salt2).hexdigest()



        instance.slug=url
        instance.save()

# a cada nota postada é atualizado a nota da resposta e a nota geral da atividade
@receiver(post_save,sender=Notas)
def handler_nota_to_resposta(sender,instance,**kwargs):

    instance.resposta.calcula_nota()
    instance.resposta.atividade.nota_total()

@receiver(post_save,sender=Criterio)
def handler_criterio(sender,instance,raw,**kwargs):


    try:
        temp=Criterio.objects.get(pk=instance.pk).peso
    except:
        temp=0



    if instance.atividade.criterio_set.aggregate(Sum('peso'))['peso__sum'] + instance.peso-temp>100:


        raise ValidationError("A soma de todos os pesos não pode ser maior que 100%")

