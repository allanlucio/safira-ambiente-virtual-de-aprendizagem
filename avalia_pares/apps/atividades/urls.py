from django.conf.urls import patterns, url


from avalia_pares.apps.atividades.views import *
from avalia_pares.apps.professor.views import baixa_atividade

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'avaliacao_pares.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^treplicar/(?P<slug>[a-zA-Z0-9-]+)$', posta_treplica,name='aluno_treplica'),
    url(r'^replica/(?P<slug>[a-zA-Z0-9-]+)$', posta_replica,name='aluno_replica'),
    url(r'^comenta/(?P<slug>[a-zA-Z0-9-]+)$', posta_comentario,name='aluno_comentario'),
    url(r'^posta/nota/(?P<slug>[a-zA-Z0-9-]+)$', posta_notas,name='aluno_posta_nota'),
    url(r'^posta/nota_avaliador/(?P<slug>[a-zA-Z0-9-]+)$', posta_nota_avaliador,name='aluno_posta_nota_avaliador'),

    url(r'^detalhes/(?P<slug>[a-zA-Z0-9-]+)$', detalhe_atividade,name='detalhe_atividade'),
    url(r'^download/(?P<slug>[a-zA-Z0-9-]+)$', baixa_atividade,name='baixa_atividade'),
    url(r'^notas/(?P<slug_atividade>[a-zA-Z0-9-]+)$',getNotasAluno,name='notas_individuais_detalhadas'),

    


)

