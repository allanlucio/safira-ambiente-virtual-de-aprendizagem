from django.contrib import admin
from avalia_pares.apps.atividades.models import *

class AdminAtividade(admin.ModelAdmin):
    model=AtividadesPeer

class AdminSub(admin.ModelAdmin):
    model=AtividadeSubmetidas

class AdminNotas(admin.ModelAdmin):
    model=Notas

class AdminCriterio(admin.ModelAdmin):
    model=Criterio

class adminResposta(admin.ModelAdmin):
    model=Resposta





admin.site.register(AtividadesPeer,AdminAtividade)
admin.site.register(AtividadeSubmetidas,AdminSub)
admin.site.register(Notas,AdminNotas)
admin.site.register(Criterio,AdminCriterio)

admin.site.register(Resposta,adminResposta)
