# # -*- coding: utf-8 -*-

from django.utils import timezone
from django.utils.safestring import mark_safe
from django import template
from django.core.urlresolvers import reverse
from avalia_pares.apps.atividades.models import AtividadesPeer

register = template.Library()

#Retorna um botão de acordo com a relação do aluno com a disciplina
@register.filter
def fase_atvd(atividade):
    atividades=AtividadesPeer.objects.get(atividade__pk=atividade)
    
    data_hoje=timezone.now()
    if data_hoje<=atividades.fase_Inicio:
        return mark_safe('<a href=\"#\" class=\"btn btn-default\">Atividade não iniciada</a>')
    elif atividades.fase_Inicio <= data_hoje <=atividades.fase_submeter:
        url = reverse(viewname='aluno_sub_atividade', kwargs={'slug_disc':atividades.disciplina.slug, 'slug':atividades.slug})
        return mark_safe('<a href=\"%s\" class=\"btn btn-warning\">Fase Submissão</a>' %url)
    elif atividades.fase_submeter <= data_hoje <= atividades.fase_comentar:
        url = reverse(viewname='lista_atividades',kwargs={'slug_disc':atividades.disciplina.slug, 'slug':atividades.slug})
        return mark_safe('<a href=\"%s\" class=\"btn btn-warning\">Fase Comentário</a>' %url)
    elif atividades.fase_comentar <= data_hoje <= atividades.fase_replica:
        url = reverse(viewname='lista_atividades',kwargs={'slug_disc':atividades.disciplina.slug, 'slug':atividades.slug})
        return mark_safe('<a href=\"%s\" class=\"btn btn-warning\">Fase Réplica</a>' %url)
    elif atividades.fase_replica <= data_hoje <= atividades.fase_treplica:
        url = reverse(viewname='lista_atividades',kwargs={'slug_disc':atividades.disciplina.slug, 'slug':atividades.slug})
        return mark_safe('<a href=\"%s\" class=\"btn btn-warning\">Fase Tréplica</a>' %url)
    elif atividades.fase_treplica <= data_hoje <= atividades.fase_nota:
        url = reverse(viewname='lista_atividades',kwargs={'slug_disc':atividades.disciplina.slug, 'slug':atividades.slug})
        return mark_safe('<a href=\"%s\" class=\"btn btn-warning\">Fase Notas</a>' %url)
    else:
        return mark_safe('<a href=\"\" class=\"btn btn-success\" disabled>Atividade finalizada</a>')


@register.filter
def data_atvd(atividade):
    atividades=AtividadesPeer.objects.get(atividade__pk=atividade)

    data_hoje=timezone.now()
    if data_hoje<=atividades.fase_Inicio:
        return "%s : %s"%(conv_date_data(atividades.fase_Inicio),conv_date_horas(atividades.fase_Inicio))
    elif atividades.fase_Inicio <= data_hoje <=atividades.fase_submeter:
        return "%s : %s"%(conv_date_data(atividades.fase_submeter),conv_date_horas(atividades.fase_submeter))
    elif atividades.fase_submeter <= data_hoje <= atividades.fase_comentar:
        return "%s : %s"%(conv_date_data(atividades.fase_comentar),conv_date_horas(atividades.fase_comentar))

    elif atividades.fase_comentar <= data_hoje <= atividades.fase_replica:
        return "%s : %s"%(conv_date_data(atividades.fase_replica),conv_date_horas(atividades.fase_replica))
    elif atividades.fase_replica <= data_hoje <= atividades.fase_treplica:
        return "%s : %s"%(conv_date_data(atividades.fase_treplica),conv_date_horas(atividades.fase_treplica))
    elif atividades.fase_treplica <= data_hoje <= atividades.fase_nota:
        return "%s : %s"%(conv_date_data(atividades.fase_nota),conv_date_horas(atividades.fase_nota))
    else:
        return "%s : %s"%(conv_date_data(atividades.fase_nota),conv_date_horas(atividades.fase_nota))

@register.assignment_tag
def fase_atual(atividade):
    atividades=AtividadesPeer.objects.get(atividade__pk=atividade)

    data_hoje=timezone.now()
    if data_hoje<=atividades.fase_Inicio:
        return False
    elif atividades.fase_Inicio <= data_hoje <=atividades.fase_submeter:
        return "submeter"
    elif atividades.fase_submeter <= data_hoje <= atividades.fase_comentar:
        return "comentar"

    elif atividades.fase_comentar <= data_hoje <= atividades.fase_replica:
        return "replicar"
    elif atividades.fase_replica <= data_hoje <= atividades.fase_treplica:
        return "treplicar"
    elif atividades.fase_treplica <= data_hoje <= atividades.fase_nota:
        return "nota"
    else:
        return "finalizada"




def conv_date_data(date):
    return date.strftime("%d/%m/%Y")

def conv_date_horas(date):
    return date.strftime("%H:%M")



