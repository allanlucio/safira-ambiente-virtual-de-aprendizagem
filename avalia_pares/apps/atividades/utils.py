__author__ = 'allan'

def media(lista):
    media=0
    for a in lista:
        media+=a
    return media/float(len(lista))

def mediana(lista):

    lista.sort()

    tamanho=len(list(lista))
    if tamanho%2==0:
        v1=lista[tamanho/2]
        v2=lista[tamanho/2-1]
        return (v1+v2)/2.0

    return lista[tamanho/2]

def conv_date_horas(date):
    return date.strftime("%H:%M")

def conv_date_data(date):
    return date.strftime("%d/%m/%Y")