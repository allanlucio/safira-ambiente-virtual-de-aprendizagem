# # -*- coding: utf-8 -*-
from django import template
from core.apps.ava.models import Disciplina

register = template.Library()

#Retorna verdadeiro se o professor for o administrador da disciplina
@register.filter()
def rel_prof_disc(user,slug):
    professor=user.professor
    if Disciplina.objects.get(slug=slug).professor == professor:
        return True
    else:
        return False







