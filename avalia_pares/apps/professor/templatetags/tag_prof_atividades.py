from core.apps.ava.models import AlunoDisciplina

__author__ = 'allan'

from django import template

register = template.Library()


@register.filter
def qtd_atividades_professor(user):
    prof=user.professor

    qtd=len(AlunoDisciplina.objects.filter(pendencia=True,disciplina__professor=prof))



    return qtd