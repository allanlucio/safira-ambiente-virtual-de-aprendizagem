# -*- coding: utf-8 -*-
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.forms import PasswordChangeForm
import json
from django.contrib.formtools.utils import form_hmac
from django.core import serializers

from django.core.urlresolvers import reverse, reverse_lazy
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

#professor
from django.utils.decorators import method_decorator
from django.utils.encoding import smart_str
from django.views.generic.edit import CreateView
from avalia_pares.apps.atividades.forms import CreateAtividade
#retorna todas as disciplinas que o professor possui
from avalia_pares.apps.atividades.models import AtividadesPeer, AtividadeSubmetidas
from avalia_pares.apps.disciplina.forms import FormDisciplinaPeer
from avalia_pares.apps.disciplina.models import DisciplinaPeer
from core.apps.autenticar.models import User
from core.apps.ava.decorators import administrador_disciplina, is_professor
from core.apps.ava.forms import AtualizarProfessor, FormDisciplina
from core.apps.ava.models import AlunoDisciplina, Aluno, Disciplina



@login_required
@is_professor()
def home_professor(request):
    dados={'alunos_pendentes':AlunoDisciplina.objects.filter(pendencia=True,disciplina__professor=request.user.professor)}
    return render(request,'home_professor.html',dados)


@login_required
@is_professor()
def disc_professor(request):

    disciplinas=DisciplinaPeer.objects.filter(disciplina__professor=request.user.professor)
    return render(request, 'professor_lista_disciplinas.html',{'disciplinas':disciplinas})

#detalha as disciplinas do professor
@login_required
@is_professor()
def disc_professor_detail(request,slug,msgNotify=False):
    print request
    disciplina=DisciplinaPeer.objects.get(disciplina__slug=slug)
    quantidade=AlunoDisciplina.objects.filter(disciplina__slug=slug,pendencia=False).count()
    quantidade_pend=AlunoDisciplina.objects.filter(disciplina__slug=slug,pendencia=True).count()
    return render(request, 'detalhes_disc_professor.html',{'msgNotify':msgNotify,'disciplina':disciplina,'qtd':quantidade,'qtd_pend':quantidade_pend})

@login_required
@is_professor()
def atualiza_perfil_professor(request):
    professor=request.user.professor
    form_user=AtualizarProfessor(request.POST or None,instance=professor)
    form=PasswordChangeForm(request.POST or None)
    contexto={}


    if request.method=='POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form_user.is_valid():
            user=form_user.save()

            contexto['message1']='Perfil atualizado com sucesso!!'
            form=PasswordChangeForm(None)
        elif form.is_valid():

            form.save()
            # Updating the password logs out all other sessions for the user
            # except the current one if
            # django.contrib.autenticar.middleware.SessionAuthenticationMiddleware
            # is enabled.
            update_session_auth_hash(request, form.user)
            contexto['message2']='Senha alterada com sucesso'
            form_user=AtualizarProfessor(None,instance=professor)

    contexto['form']=form
    contexto['user_form']=form_user

    return render(request,'prof_atualiza_perfil.html',contexto)





#Retorna todos os alunos pendentes.
#POST = será possível aceitar ou negar a particiṕação do aluno na disciplina
@login_required
@is_professor()
def disc_aluno_pendente(request,slug):

    #aceitar ou negar a participação via post
    if request.method == 'POST':
        data=request.POST
        id=data['dado']
        if data['resposta'] == 'True':
            pend=AlunoDisciplina.objects.get(aluno=id,disciplina__slug=slug)
            pend.pendencia=False
            pend.save()
            return HttpResponse("Aceito com sucesso!")
        elif data['resposta']=="False":
            pend=AlunoDisciplina.objects.get(aluno=id,disciplina__slug=slug)
            pend.delete()
            return HttpResponse("Removido com sucesso!")



    alunos=AlunoDisciplina.objects.filter(disciplina__professor=request.user.professor,pendencia=True)
    return render(request, 'prof_disc_alunos.html',{'alunos':alunos})


#Retorna todos os alunos matriculados e da a opção do professor remove-los ou não da disciplina
#POST dado= id do aluno e resposta= True(aceita)/False(Nega)
# slug = slug da disciplina
@login_required
@permission_required("professor.view_professor")
@administrador_disciplina
def disc_aluno_mat(request,slug):
    disciplina=DisciplinaPeer.objects.get(slug=slug)
    alunos=AlunoDisciplina.objects.filter(disciplina=disciplina,pendencia='Nao')
    if request.method == 'POST':
        data=request.POST
        id=data['dado']
        if data['resposta'] == 'True':
            pend=AlunoDisciplina.objects.get(aluno=id,disciplina__slug=slug)
            pend.pendencia="Nao"
            pend.save()
            return HttpResponse("Aceito com sucesso!")
        elif data['resposta']=="False":
            pend=AlunoDisciplina.objects.get(aluno=id,disciplina__slug=slug)
            pend.delete()
            return HttpResponse("Removido com sucesso!")
    return render(request, 'home',{'alunos':alunos})

@login_required
@is_professor()
@administrador_disciplina
def aceitar_negar_aluno(request,slug):
    if request.method == 'POST':
        data=request.POST
        id=data['dado']
        if data['resposta'] == 'True':
            pend=AlunoDisciplina.objects.get(aluno=id,disciplina__slug=slug)
            pend.pendencia=False
            pend.save()
            return HttpResponse("Aceito com sucesso!")
        elif data['resposta']=="False":
            pend=AlunoDisciplina.objects.get(aluno=id,disciplina__slug=slug)
            pend.delete()
            return HttpResponse("Removido com sucesso!")



#Class View para criação de disciplinas baseada em um professor.
@login_required()
@is_professor()
def cria_Disciplina(request):
    form_DiscPeer=FormDisciplinaPeer(request.POST or None)
    form_Disciplina=FormDisciplina(request.POST or None)
    msgNotify=False
    msgNotifyErr=False
    if request.method=='POST':
        if form_Disciplina.is_valid() and form_DiscPeer.is_valid():
            #processo para a disciplina do AVA
            professor=request.user.professor
            disc=form_Disciplina.save(commit=False)
            disc.professor=professor
            disc.save()
            #Processo paraa disciplina do peer
            discPeer=form_DiscPeer.save(commit=False)
            discPeer.disciplina=disc
            discPeer.save()

            msgNotify="Dados Salvos com Sucesso!!"


            return redirect(reverse(viewname='professor_disciplinas_detail',kwargs={'slug':disc.slug}))

        else:
            msgNotifyErr="Corrija os erros antes de submeter novamente"



    return render(request,'criar_disciplina.html',{'form_Peer':form_DiscPeer,'form_Disc':form_Disciplina,'msgNotify':msgNotify,'msgNotifyErr':msgNotifyErr})

@login_required
@is_professor()
@administrador_disciplina
def Edita_Disciplina(request,slug):

    disciplina=DisciplinaPeer.objects.get(disciplina__slug=slug)

    form_DiscPeer=FormDisciplinaPeer(request.POST or None,instance=disciplina)
    form_Disciplina=FormDisciplina(request.POST or None,instance=disciplina.disciplina)
    if request.method == 'POST':
        if form_DiscPeer.is_valid() and form_Disciplina.is_valid():
            form_Disciplina.save()
            form_DiscPeer.save()
            data=json.dumps({'resposta':True})
            return HttpResponse(data,content_type='application/json')



    return render(request, 'editar_disciplina.html',{'form_Peer':form_DiscPeer,'form_Disc':form_Disciplina})

@login_required
@is_professor()
def Edita_Atividade(request,slug):

    atividade=AtividadesPeer.objects.get(atividade__slug=slug)
    # if request.FILES:
    #             atividade.arquivo.delete()

    form=CreateAtividade(files=request.FILES or None,data=request.POST or None,instance=atividade)

    if request.method == 'POST':
        if form.is_valid():

            form.save()

            return redirect(reverse(viewname='Edita_Atividade',kwargs={'slug':slug}))

    return render(request, 'editar_atividade.html',{'form':form})

@login_required
@is_professor()
def baixa_atividade(request,slug):
    print 'oi'
    atvd=AtividadesPeer.objects.get(atividade__slug=slug)


    response = HttpResponse(atvd.arquivo,content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename=%s' % smart_str(atvd.arquivo.name)



    # Create the PDF object, using the response object as its "file."


    # It's usually a good idea to set the 'Content-Length' header too.
    # You can also set any other required headers: Cache-Control, etc.
    return response

#Lista todas as atividades existentes em ordem alfabetica
@login_required
@is_professor()
def Lista_Atividades(request):

    atividades=AtividadesPeer.objects.filter(atividade__disciplina__professor=request.user.professor)

    return render(request, 'prof_atividades.html',{'atividades':atividades})

#Criar uma atividade, precisa de algumas modificações mas a idéia básica é essa.
class Cria_Atividade(CreateView):
    model = AtividadesPeer

    template_name = 'criar_atividade.html'
    context_object_name = 'form'

    slug_field = 'slug'
    slug=None

    

    @method_decorator(login_required())
    @method_decorator(is_professor)
    def post(self, request, *args, **kwargs):
        slug=str(kwargs['slug'])
        arquivo=request.FILES['arquivo']
        titulo=request.POST['titulo']
        disciplina=get_object_or_404(DisciplinaPeer,slug=slug)
        atividade=AtividadesPeer(atividade__titulo=titulo,atividade__arquivo=arquivo,atividade__disciplina=disciplina)
        atividade.save()
        return HttpResponse("Atividade criada com sucesso!")

    @method_decorator(login_required)
    @method_decorator(is_professor)
    def dispatch(self, request, *args, **kwargs):
        return super(Cria_Atividade,self).dispatch(request,*args,**kwargs)

@login_required
@is_professor()
#Nova versao do criar disciplina, com codigo muito mais enchuto
def cria_atividade(request,slug):

    disciplina=DisciplinaPeer.objects.get(disciplina__slug=slug)
    # atividade=Atividades()
    # atividade.disciplina=disciplina
    # atividade.save()
    print request.FILES
    form=CreateAtividade(data=request.POST or None,files=request.FILES or None)

    if request.method == 'POST':


        if form.is_valid():
            print "valido"

            atvd=form.save(commit=False)
            atvd.atividade.disciplina=disciplina
            atvd.save()

            return render(request, 'confirmacaoAtv.html')
        else:
            HttpResponse('invalido')


    return render(request, 'criar_atividade.html',{'form':form})

@login_required
@is_professor()
def alocar_trabalhos(request,slug):
    atv = AtividadeSubmetidas.objects.filter(atividade__slug=slug)

    atv[0].atividade.aloca_atividades()
    return render(request, 'alocar_trabalhos.html',{'atividades':atv})


login_required()
@is_professor()
def excluir_atividade(request,slug):
    atividade= AtividadesPeer.objects.get(atividade__slug=slug)
    if request.method=='POST':
        atividade.delete()

        return redirect(reverse(viewname='Lista_Atividades'))

    return render_confirm(request,'Lista_Atividades',"Você deseja excluir a atividade: ",atividade.titulo)


def render_confirm(request,viewname,titulo,objeto_nome):


    return render(request, 'confirmar.html',{'link':reverse(viewname=viewname),'titulo':titulo,'objeto':objeto_nome})



#Pagina que exibe a nota final de cada aluno dentro da atividade

@login_required()
@is_professor()
def nota_atividade_alunos(request,slug_atividade):
    atividade_sub=AtividadeSubmetidas.objects.filter(atividade__slug=slug_atividade)

    return render(request,'Notas_Alunos_atividades.html',{'atividades':atividade_sub})



#gerar atividades automaticamente
def geraAtividades():
    disc=DisciplinaPeer.objects.get(disciplina__slug="teste-alocar-20141-1")
    atvd=AtividadesPeer.objects.get(atividade__slug="testando-forte-teste-alocar-1")
    alunos=disc.alunodisciplina_set.filter(pendencia=False)
    for al in alunos:
        atvs=AtividadeSubmetidas(aluno=al.aluno,atividade=atvd,arquivo=atvd.arquivo)
        atvs.save()
