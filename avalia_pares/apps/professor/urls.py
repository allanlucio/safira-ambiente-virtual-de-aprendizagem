from django.conf.urls import patterns, url

from avalia_pares.apps.professor.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'avaliacao_pares.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


#professor
    url(r'^home$',home_professor ,name='home_professor'),
    url(r'^disciplinas$', disc_professor,name='professor_disciplinas'),
    url(r'^disciplina/(?P<slug>[a-zA-Z0-9-]+)$', disc_professor_detail,name='professor_disciplinas_detail'),#Ver detalhes da disciplina
    url(r'^disciplina/pendente/(?P<slug>[a-zA-Z0-9-]+)$', disc_aluno_pendente,name='disc_aluno_pend'),#Retorna e deixa gerenciar alunos pendentes
    url(r'^disciplina/solicitou/(?P<slug>[a-zA-Z0-9-]+)$', aceitar_negar_aluno,name='aceitar_negar_aluno'),#Retorna e deixa gerenciar alunos pendentes
    url(r'^disciplina/matriculado/(?P<slug>[a-zA-Z0-9-]+)$', disc_aluno_mat,name='disc_aluno_mat'),
    url(r'^criar/disciplina',cria_Disciplina,name='professor_cria_disciplina'),
    url(r'^criar/atividade/(?P<slug>[a-zA-Z0-9-]+)$',cria_atividade,name='professor_cria_atividade'),#Cria uma atividade relacionado a uma disciplina
    url(r'^atualizar_dados',atualiza_perfil_professor,name='atualiza_dados_professor'),#atualizar os dados
    url(r'^editar/disciplina/(?P<slug>[a-zA-Z0-9-]+)$',Edita_Disciplina,name='Edita_Disciplina'),
    url(r'^editar/atividade/(?P<slug>[a-zA-Z0-9-]+)$',Edita_Atividade,name='Edita_Atividade'),
    url(r'^atividades$',Lista_Atividades,name='Lista_Atividades'),
    url(r'^atividades/excluir/(?P<slug>[a-zA-Z0-9-]+)$', excluir_atividade,name='excluir_atividade'),
    url(r'^atividades/notas/(?P<slug_atividade>[a-zA-Z0-9-]+)$', nota_atividade_alunos,name='nota_atividade'),

    url(r'^alocar/(?P<slug>[a-zA-Z0-9-]+)$',alocar_trabalhos,name='alocar_trabalhos')#Aloca os trabalhos caso seja chamada

)
