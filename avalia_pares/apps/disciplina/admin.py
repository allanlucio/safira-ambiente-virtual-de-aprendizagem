from django.contrib import admin

from avalia_pares.apps.disciplina.models import *


class disciplinaAdmin(admin.ModelAdmin):
    model=DisciplinaPeer
    #list_display = ('nome','semestre','data_criacao',)
    #search_fields = ('nome',)
    #list_filter = ('data_criacao','semestre',)

admin.site.register(DisciplinaPeer,disciplinaAdmin)