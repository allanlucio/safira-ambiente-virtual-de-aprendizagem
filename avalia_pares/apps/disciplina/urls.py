from django.conf.urls import patterns, url

from avalia_pares.apps.disciplina.views import *

urlpatterns = patterns('',

    # url(r'^disciplinas$', listaDisciplina,name='disciplinas'),
    url(r'^disciplina/detalhes/(?P<slug>[a-zA-Z0-9-]+)$', detalhe_disciplina,name='detalhe_disciplinas'),
    url(r'^disciplina/solicitar/(?P<slug>[a-zA-Z0-9-]+)$', solicitaDisciplina,name='solicita_disciplina'),
    url(r'^disciplina/excluir/(?P<slug>[a-zA-Z0-9-]+)$', excluir_disciplina,name='excluir_disciplina'),









)
