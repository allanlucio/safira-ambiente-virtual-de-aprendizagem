# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect


#Detalhes das disciplinas, bem como se receber uma requisição post, realiza o pedido de matricula na disciplina
from avalia_pares.apps.professor.views import render_confirm
from core.apps.ava.decorators import administrador_disciplina, is_aluno, is_professor
from core.apps.ava.models import Disciplina, AlunoDisciplina


@login_required
@is_aluno() #Mudar view
def detalhe_disciplina(request,slug):
    disciplina=Disciplina.objects.get(slug=slug)





    return render(request, 'detalhe_disc.html',{'disciplina':disciplina})

@login_required()
@is_aluno()
def solicitaDisciplina(request,slug):
    disciplina=Disciplina.objects.get(slug=slug)
    aluno=AlunoDisciplina(disciplina=disciplina,aluno=request.user.aluno)
    msg=aluno.save_securit()
    return HttpResponseRedirect(reverse(viewname='disciplinas'))



# #Lista todas as disciplinas existentes em ordem alfabetica
# @login_required
# def listaDisciplina(request):
#
#
#     disciplinas=Disciplina.objects.all()
#
#     return render(request, 'disciplinas.html',{'disciplinas':disciplinas})





#baseado em um slug uma disciplina pode ser excluida chamando esse metodo
@login_required()
@is_professor()
#@administrador_disciplina
def excluir_disciplina(request,slug):
    print 'oi'
    disciplina= Disciplina.objects.get(slug=slug)
    if request.method=='POST':

        disciplina.delete()
        return redirect(reverse(viewname="professor_disciplinas"))

    return render_confirm(request,'professor_disciplinas',"Você deseja excluir a Disciplina: ",disciplina.nome)









