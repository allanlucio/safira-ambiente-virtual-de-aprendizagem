# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver
from django.template.defaultfilters import slugify


from core.apps.ava.models import Disciplina


def validaPenalidade(value):
    if value<0 or value >100:
        raise ValidationError("A penalidade deve ter valores entre 0 e 100. voce deu como entrada o valor %s:"%value)

def valida_num_avaliadores(value):
    if value %2==0:
        raise ValidationError("O numero de avaliadores deve ser ímpar!")

class DisciplinaPeer(models.Model):


    disciplina=models.ForeignKey(Disciplina,unique=True)
    penalidade=models.FloatField(validators=[validaPenalidade],help_text="Digite valores entre 0% e 100%")
    quantidade_de_avaliadores=models.IntegerField(verbose_name="Quantidade de avaliadores",max_length=1,blank=True,default=3,help_text="<b>Lembre-se:</b> O numero de avaliadores deve ser um numero <b>ímpar</b> menor ou igual ao número de alunos ")
    email_disciplina=models.EmailField(max_length=120,null=True,blank=True,help_text="Email para o aluno entrar em contato")

    def __unicode__(self):
        return self.disciplina.nome

    def get_atividades(self):

        return self.atividades_set.all()

    def get_detalhe_disciplina(self):
        return reverse(viewname='detalhe_disciplinas',kwargs={'slug':self.slug})

    def get_absolute_url_aluno(self):
        return reverse(viewname='aluno_disciplina',kwargs={'slug':self.slug})

    def get_absolute_url_atividades(self):
        return reverse(viewname='aluno_disc_atividades',kwargs={'slug_disc':self.slug})

    def get_url_detail_prof(self):
        return reverse(viewname='professor_disciplinas_detail',kwargs={'slug':self.disciplina.slug})

    def get_url_alunos_pendentes(self):
        return reverse(viewname='disc_aluno_pend',kwargs={'slug':self.slug})
    def get_url_alunos_matriculados(self):
        return reverse(viewname='disc_aluno_mat',kwargs={'slug':self.slug})



