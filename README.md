# README #


#**Aviso:** Faça checkout na Branch av_pares, caso queira utilizar o Safira. #

Link para download direto da aplicação: [Download](https://bitbucket.org/plataforma-exercicios/safira-ambiente-virtual-de-aprendizagem/get/av-pares.zip)


### Ambiente de Avaliação (Avaliação por Pares) ###

* v2.0
* O ambiente está Atualmente sendo modificado, para suportar um protocolo dinâmico de etapas durante as atividades.

### Visão Geral ###
O sistema em questão é um Ambiente de Avaliação(AVA), que foi implementado para executar na WEB Utilizando Django Framework. Por se tratar de um AVA o mesmo possui algumas características básicas, como os módulos de: Professor, Usuário, Aluno, Disciplinas e Atividades. Mas como o mesmo também contempla um Módulo de Avaliação por Pares(AP), Também conta com toda à lógica de negócio para o seu pleno funcionamento.

### Depêndencias ###

* Python 2.7.6
* Django 1.7.5
* Pillow PIL
* Django-Bootstrap3
* Mysql Server
###Base de Dados###
Mysql Server:
* Criar uma Tabela com o nome *peer*

## Executanto o projeto ##

Sequência padrão de execução de projetos Django

*  Crie e migre a database uma database: **python manage.py migrate**
*  Crie um super usuário: **python manage.py create superuser**
*  Execute o projeto: **python manage.py runserver**